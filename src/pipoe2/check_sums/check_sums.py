"""Module for calculating check hashes."""

from pathlib import Path
import hashlib
from functools import partial


def md5sum(path: Path) -> str:
    """Calculate MD5 check sum."""
    with path.open("rb") as file:
        digest = hashlib.md5()
        for buf in iter(partial(file.read, 128), b""):
            digest.update(buf)

    return digest.hexdigest()


def sha256sum(path: Path) -> str:
    """Calculate SHA256 check sum."""
    with path.open("rb") as file:
        digest = hashlib.sha256()
        for buf in iter(partial(file.read, 128), b""):
            digest.update(buf)

    return digest.hexdigest()
