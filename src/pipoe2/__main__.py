"""CLI entrypoint for pipoe2 package."""

from .pipoe2 import main

if __name__ == "__main__":
    main()
