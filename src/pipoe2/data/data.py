"""Models connected to store data about packages."""

from dataclasses import dataclass, field
from enum import Enum


@dataclass
class Author:
    """Author's metadata."""

    name: str
    email: str


@dataclass
class Description:
    """Package documentation metadata."""

    homepage: str
    summary: str


class BuiltType(str, Enum):
    """Type of built type for the package."""

    SETUPTOOLS = "setuptools3"
    SETUPTOOL_META = "python_setuptools_build_meta"
    POETRY = "python_poetry_core"
    FLINT = "python_flint_core"


@dataclass
class LicenseData:
    """Information about license."""

    name: str
    file_name: str
    md5sum: str


@dataclass
class ArchiveData:
    """Metadata about archive."""

    tar_name: str
    tar_url: str
    md5sum: str
    sh256sum: str
    source_repository_url: str


@dataclass
class Dependencies:
    """Metadata about package dependencies."""

    build: list[tuple[str, str]] = field(default_factory=list)
    runtime: list[str] = field(default_factory=list)


@dataclass
class Package:
    """Main package metadata class."""

    name: str
    version: str
    author: Author
    description: Description
    build_tool: BuiltType
    license: LicenseData
    archive: ArchiveData
    dependencies: Dependencies
