"""Templates for the recipes."""

BB_PYPI_RECIPE_TEMPLE = """\
SUMMARY = "{summary}"
HOMEPAGE = "{homepage}"
AUTHOR = "{author} <{author_email}>"
LICENSE = "{license}"
LIC_FILES_CHKSUM = "file://{license_file};md5={license_md5}"

inherit setuptools3 pypi

SRC_URI[md5sum] = "{md5sum}"
SRC_URI[sha256sum] = "{sha256sum}"

DEPENDS += " {build_dependencies}"
RDEPENDS:${{PN}} = "{runtime_dependencies}
"
"""

BB_GIT_RECIPE_TEMPLE = """\
SUMMARY = "{summary}"
HOMEPAGE = "{homepage}"
AUTHOR = "{author} <{author_email}>"
LICENSE = "{license_name}"
LIC_FILES_CHKSUM = "file://{license_file};md5={license_md5}"

inherit setuptools3

SRC_URI += " \\
    git://{source_code_url}.git;protocol=https \\
"
SRCREV = ""

DEPENDS += " {build_dependencies}"
RDEPENDS:${{PN}} = "{runtime_dependencies}
"
"""
