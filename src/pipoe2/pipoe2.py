"""New pipoe module to generate BitBake recipes for PyPi packages"""

import json
from typing import Any, Union
import urllib.request
from pathlib import Path
import tempfile
import tarfile
import os
import re
import sys
from unittest import mock
import setuptools


from . import licenses
from .check_sums import check_sums
from .data import data
from .templates.templates import BB_PYPI_RECIPE_TEMPLE


def get_packages_from_file(path: Path) -> list[tuple[str, str]]:
    """Read packages from requirements file."""
    content = path.read_text().splitlines()
    return [tuple(line.split("==")) for line in content if "#" not in line]


def build_json_url(package_name: str, version: Union[str, None] = None) -> str:
    """Build package PyPi url."""
    if version:
        return f"https://pypi.org/pypi/{package_name}/{version}/json"

    return f"https://pypi.org/pypi/{package_name}/json"


def get_json_data(url: str) -> dict[str, Any]:
    """Gets JSON data from given url."""
    with urllib.request.urlopen(url) as file:
        response = file.read().decode(encoding="UTF-8")

    return json.loads(response)


def find_matching(pattern: str, path: Path) -> list[Path]:
    """Find files matching given pattern."""
    result = []

    rex = re.compile(pattern, flags=re.I | re.X)

    for root, _, files in os.walk(path):
        for name in files:
            file = rex.findall(name)
            if file:
                result.append(Path(root) / name)

    return result


def translate_license(pypi_license: str, default: str = "MIT") -> str:
    """Translates development license to Yocto licenses."""
    try:
        return licenses.LICENSES[pypi_license]
    except KeyError:
        return default


def download_tar_from_url(url: str, path: Path) -> None:
    """Downloads tar.gz file from given url."""
    urllib.request.urlretrieve(url, path)


def find_license_file(path: Path) -> Path:
    """Finds LICENSE file in path."""
    regrex = r"\bLICENSE\b | \bCOPYING\b | \bLICENCE\b"
    license_files = find_matching(regrex, path)

    if not license_files:
        license_files = find_matching(r"\bsetup.py\b | \bpyproject.toml\b", path)

    return license_files[0]


def find_dependencies_setuptools(setup_file: Path) -> list[str]:
    """Find dependencies from setup.py"""
    current_pwd = os.getcwd()
    package_path = str(setup_file.parent.absolute())
    os.chdir(package_path)
    sys.path.append(package_path)
    with mock.patch.object(setuptools, "setup") as mock_setup:
        import setup  # This is setup.py which calls setuptools.setup

    args, kwargs = mock_setup.call_args
    print(
        f"{setup_file.parent.name} -- setup_requires-- {kwargs.get('setup_requires', [])}"
    )
    sys.path.remove(package_path)
    os.chdir(current_pwd)


def save_license_information_for_package(package: data.Package) -> None:
    """Get information about package LICENSE from tar file."""
    with tempfile.TemporaryDirectory() as temporary_dir:
        downloaded_tar = Path(temporary_dir) / package.archive.tar_name
        downloaded_dir = Path(temporary_dir) / f"{package.archive.tar_name}.d"
        download_tar_from_url(package.archive.tar_url, downloaded_tar)

        with tarfile.open(downloaded_tar) as tar_file:
            tar_file.extractall(downloaded_dir)

        license_file = find_license_file(downloaded_dir)
        package.license.file_name = license_file.name
        package.license.md5sum = check_sums.md5sum(license_file)

        downloaded_tar.unlink()


def parse_requirements() -> list[str]:
    """Parses package requirements."""


def extract_package_information(content: dict[str, Any]) -> data.Package:
    """Extracts metadata information form json response."""
    source_code = ""
    md5sum = ""
    sha256sum = ""
    package_url = ""

    info = content["info"]

    try:
        for url in info["project_urls"]:
            source_code = (
                info["project_urls"][url]
                if "github" in info["project_urls"][url]
                else ""
            )
    except (KeyError, TypeError):
        pass  # Check if source distribution

    for url in content["urls"]:
        if url["packagetype"] != "sdist":
            continue

        md5sum = url["digests"]["md5"]
        sha256sum = url["digests"]["sha256"]
        package_url = url["url"]

    if not source_code and not md5sum:
        raise Exception("Project has no url to get source code")

    dependencies = data.Dependencies()
    if info["requires_dist"]:
        for package in info["requires_dist"]:
            dependencies.runtime.append(package.split(" ;")[0])

    return data.Package(
        name=info["name"],
        version=info["version"],
        author=data.Author(info["author"], info["author_email"]),
        description=data.Description(info["home_page"], info["summary"]),
        license=data.LicenseData(translate_license(info["license"]), "", ""),
        build_tool=data.BuiltType.SETUPTOOLS,
        archive=data.ArchiveData(
            package_url.split("/")[-1],
            package_url,
            md5sum,
            sha256sum,
            source_code.lstrip("https://"),
        ),
        dependencies=dependencies,
    )


def package_to_bb_name(name: str) -> str:
    """Translate pypi package name to bitbake recipe compatible name"""
    return name.lower().replace("_", "-").replace(".", "-")


def format_package_dependencies(dependencies: list[str]) -> str:
    """Format package dependencies into Yocto format."""
    result = " \\"
    for package in dependencies:
        try:
            name, version = package.split(" ")
        except ValueError:
            name = package
            version = ""

        if version:
            found = re.search(">=[0-9].[0-9].[0-9]", version)
            if found:
                version = f"({found.group()})"

        result += f"\n    ${{PYTHON_PN}}-{package_to_bb_name(name)} {version} \\"

    return result


def main() -> None:
    """Main script entrypoint."""
    package_name, version = sys.argv[1].split("==")
    pypi_package_url = build_json_url(package_name, version)
    package_pypi_data = get_json_data(pypi_package_url)
    package_data = extract_package_information(package_pypi_data)
    if not package_data.archive.md5sum:
        return

    save_license_information_for_package(package_data)

    print("-------------------------------------------------------------------")
    print(
        BB_PYPI_RECIPE_TEMPLE.format(
            author=package_data.author.name,
            author_email=package_data.author.email,
            summary=package_data.description.summary,
            homepage=package_data.description.homepage,
            license=package_data.license.name,
            license_file=package_data.license.file_name,
            license_md5=package_data.license.md5sum,
            md5sum=package_data.archive.md5sum,
            sha256sum=package_data.archive.sh256sum,
            build_dependencies="",
            runtime_dependencies=format_package_dependencies(
                package_data.dependencies.runtime
            ),
        )
    )
    # else:
    #     print(
    #         # BB_GIT_RECIPE_TEMPLE.format(
    #         #     summary=package_data.summary,
    #         #     homepage=package_data.homepage,
    #         #     author=package_data.author,
    #         #     author_email=package_data.author_email,
    #         #     license=package_data.license_name,
    #         #     license_file=package_data.license_file,
    #         #     license_md5=package_data.license_md5,
    #         #     source_code_url=package_data.source_code_url.lstrip("https://"),
    #         #     build_dependencies="",
    #         #     dependencies="",
    #         # )
    #     )
